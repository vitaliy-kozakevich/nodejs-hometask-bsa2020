const { body } = require('express-validator');
const FightService = require('../services/fightService');
const FighterService = require('../services/fighterService');
const { fight } = require('../models/fight');
const { RES_CODES } = require('../constants');

async function validateBody(...reqParams) {
  const [req, id] = reqParams;
  const reqKeys = Object.keys(req.body);
  const fightKeys = Object.keys(fight);

  for (const key of reqKeys) {
    if (!fightKeys.includes(key)) delete req.body[key];
  }

  if (id) delete req.body.id;
  if (id) {
    if (id !== req.body.id) {
      return Promise.reject(`Fight has not been found by id: ${id}!`);
    }
  }

  {
    const { fighter1: id } = req.body;
    const fighter_1 = await FighterService.search({ id });

    if (!fighter_1) {
      return Promise.reject('Please define correct first fighter id!');
    }
  }

  {
    const { fighter2: id } = req.body;
    const fighter_2 = await FighterService.search({ id });

    if (!fighter_2) {
      return Promise.reject('Please define correct second fighter id!');
    }
  }

  const { log } = req.body;
  if (!Array.isArray(log)) {
    return Promise.reject('Log property must be array!');
  } else if (log.length) {
    log.forEach((el) => {
      if (typeof el !== 'object') {
        return Promise.reject('Each log element must be object!');
      }
    });
  }
}

const createFightValid = async (req, res, next) => {
  // TODO: Implement validatior for fighter entity during creation
  try {
    await validateBody(req);
    next();
  } catch (error) {
    res.status(RES_CODES.bad_request).send({ error: true, message: error });
  }
};

const updateFightValid = async (req, res, next) => {
  // TODO: Implement validatior for fighter entity during update
  try {
    const { id } = req.params;
    await validateBody(req, id);
    next();
  } catch (error) {
    res.status(RES_CODES.bad_request).send({ error: true, message: error });
  }
};

exports.createFightValid = createFightValid;
exports.updateFightValid = updateFightValid;
