const { body } = require('express-validator');
const { user } = require('../models/user');
const UserService = require('../services/userService');
const { RES_CODES } = require('../constants');

async function validateReqBody(...reqParams) {
  const [req, user, next, id] = reqParams;
  const reqKeys = Object.keys(req.body);
  const userKeys = Object.keys(user);

  console.log(reqParams)

  for (const key of reqKeys) {
    if (!userKeys.includes(key)) delete req.body[key];
  }
  if (id) delete req.body.id;
  if (id) {
    if (id !== req.body.id) {
      return Promise.reject(`User has not been found by id: ${id}!`);
    }
  }

  const { email, phoneNumber } = req.body;
  const isValidEmail = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@(gmail)+\.([a-z]{2,4})$/.test(email);
  const isValidNumber = /^\+380\d{9}$/.test(phoneNumber);
  if (isValidEmail) {
    if (isValidNumber) {
      next();
    } else {
      return Promise.reject('Type correct phone number');
    }
  } else {
    return Promise.reject('Type correct email');
  }
}

const createUserValid = async (req, res, next) => {
  // TODO: Implement validatior for user entity during creation
  try {
    await validateReqBody(req, user, next);
  } catch (error) {
    res.status(RES_CODES.bad_request).send({ error: true, message: error });
  }
};

const updateUserValid = async (req, res, next) => {
  // TODO: Implement validatior for user entity during update
  try {
    const { id } = req.params;
    await validateReqBody(req, user, next, id);
  } catch (error) {
    res.status(RES_CODES.bad_request).send({ error: true, message: error });
  }
};

exports.registerValidators = [
  body('email').custom(async (value, { req }) => {
    try {
      const user = await UserService.search({ email: value });
      if (user) {
        return Promise.reject('Such email already exists');
      }
    } catch (error) {
      console.log(error);
    }
  }),
  body('password')
    .isLength({ min: 6 })
    .withMessage('Minimal password length — 6 symbols')
    .isAlphanumeric()
    .trim(),
  body('firstName').isLength({ min: 3 }).withMessage('First name must be 3 symbols or more').trim(),
  body('lastName').isLength({ min: 3 }).withMessage('Last name must be 3 symbols or more').trim(),
];
exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
