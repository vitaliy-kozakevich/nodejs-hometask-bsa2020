const responseMiddleware = (req, res, next) => {
  // TODO: Implement middleware that returns result of the query
  if (res.data) {
    res.status(res.stat).send(res.data);
  } else if (res.err) {
    res.status(res.stat).send({ error: true, message: res.err });
  }
};

exports.responseMiddleware = responseMiddleware;
