const { body } = require('express-validator');
const FighterService = require('../services/fighterService');
const { fighter } = require('../models/fighter');
const { RES_CODES } = require('../constants');

async function validateBody(...reqParams) {
  const [req, id] = reqParams;
  const reqKeys = Object.keys(req.body);
  const fighterKeys = Object.keys(fighter);

  console.log(reqParams)
  for (const key of reqKeys) {
    if (!fighterKeys.includes(key)) delete req.body[key];
  }

  if (!id) delete req.body.id;
  if (!req.body.health) req.body.health = fighter.health;
  if (id) {
    if (id !== req.body.id) {
      return Promise.reject(`Fighter has not been found by id: ${id}!`);
    }
  }

  const { name, power, defense } = req.body;

  if (!name || !power || !defense) {
    return Promise.reject('Please define all fighter characteristics!');
  }

  if (!Number.isInteger(power) || power > 100 || power <= 0) {
    return Promise.reject('Please type fighter power from 1 to 100!');
  }

  if (!Number.isInteger(defense) || defense > 10 || defense < 1) {
    return Promise.reject('Please type fighter defense from 1 to 10 !');
  }
}

const createFighterValid = async (req, res, next) => {
  // TODO: Implement validatior for fighter entity during creation
  try {
    await validateBody(req);
    next();
  } catch (error) {
    res.status(RES_CODES.bad_request).send({ error: true, message: error });
  }
};

const updateFighterValid = async (req, res, next) => {
  // TODO: Implement validatior for fighter entity during update
  try {
    const { id } = req.params;
    await validateBody(req, id);
    next();
  } catch (error) {
    res.status(RES_CODES.bad_request).send({ error: true, message: error });
  }
};

exports.isNameExistValidator = [
  body('name').custom(async (value, { req }) => {
    try {
      const fighter = await FighterService.search({ name: value });
      if (fighter) {
        return Promise.reject('Fighter with such name already exists');
      }
    } catch (error) {
      console.log(error);
    }
  }),
];
exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
