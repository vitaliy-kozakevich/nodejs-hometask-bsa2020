import React, { Component } from 'react';
import { createFight } from '../../services/domainRequest/fightRequest';
import Arena from '../arena';
import { controls } from './controls';
const {
  PlayerOneAttack,
  PlayerOneBlock,
  PlayerTwoAttack,
  PlayerTwoBlock,
  PlayerOneCriticalHitCombination,
  PlayerTwoCriticalHitCombination,
} = controls;

class Hits extends Component {
  state = {
    player1: this.props.fighterOne,
    player2: this.props.fighterTwo,
    leftFighter: null,
    rightFighter: null,
    leftHealth: null,
    rightHealth: null,
    pressedKeys: new Set(),
    damageToLeftFighter: 0,
    damageToRightFighter: 0,
    leftHealthValue: 100,
    rightHealthValue: 100,
    interval: 10,
  };

  componentDidMount = async () => {
    const { player1, player2 } = this.state;
    await createFight({ fighter1: player1.id, fighter2: player2.id, log: [] });
    this.setState({
      leftFighter: document.querySelector('.arena___left-fighter'),
      rightFighter: document.querySelector('.arena___right-fighter'),
      leftHealth: document.getElementById('left-fighter-indicator'),
      rightHealth: document.getElementById('right-fighter-indicator'),
    });
  };

  getDamage = (attacker, defender) => {
    // return damage
    const damage = defender >= attacker ? 0 : attacker - defender;
    return damage;
  };

  getHitPower = (fighter) => {
    // return hit power
    const criticalHitChance = 1 + Math.random() * (2 + 1 - 1);
    const powerHit = fighter.attack * criticalHitChance;
    return powerHit;
  };

  getBlockPower = (fighter) => {
    // return block power
    const dodgeChance = 1 + Math.random() * (2 + 1 - 1);
    const powerBlock = fighter.defense * dodgeChance;
    return powerBlock;
  };

  getCriticalHit = (fighter) => {
    // return critical hit power
    const criticalHit = fighter.attack * 2;
    return criticalHit;
  };

  animateAttack = (element, className) => {
    element.classList.toggle(className);
    setTimeout(() => {
      element.classList.toggle(className);
    }, 2100);
  };

  setHealth = (fighter, damage, el, value) => {
    fighter.health = fighter.health - damage;
    el.style.width = `${value}%`;
  };

  hit = (firstFighter, secondFighter) => {
    const { rightHealthValue, leftHealthValue, pressedKeys } = this.state;
    if (pressedKeys.has(PlayerOneAttack) && !pressedKeys.has(PlayerOneBlock)) {
      const damage = this.getDamage(
        this.getHitPower(firstFighter),
        this.getBlockPower(secondFighter)
      );
      const healthValue = rightHealthValue - (damage / rightHealthValue) * 100;
      this.setState({ damageToRightFighter: damage, rightHealthValue: healthValue });
      this.animateAttack(this.leftFighter, 'animate');
    } else if (pressedKeys.has(PlayerTwoAttack) && !pressedKeys.has(PlayerTwoBlock)) {
      const damage = this.getDamage(
        this.getHitPower(secondFighter),
        this.getBlockPower(firstFighter)
      );
      const healthValue = leftHealthValue - (damage / leftHealthValue) * 100;
      this.setState({ damageToLeftFighter: damage, leftHealthValue: healthValue });
      this.animateAttack(this.rightFighter, 'animate-right');
    }
  };

  controlHealth = (firstFighter, secondFighter) => {
    const {
      pressedKeys,
      damageToRightFighter,
      rightHealthValue,
      damageToLeftFighter,
      leftHealthValue,
      leftHealth,
      rightHealth,
    } = this.state;
    if (pressedKeys.has(PlayerTwoAttack) && pressedKeys.has(PlayerOneBlock)) {
      this.setHealth(firstFighter, damageToLeftFighter, leftHealth, leftHealthValue);
    } else if (pressedKeys.has(PlayerOneAttack) && pressedKeys.has(PlayerTwoBlock)) {
      this.setHealth(secondFighter, damageToRightFighter, rightHealth, rightHealthValue);
    }
  };

  criticalHit = (firstFighter, secondFighter, code) => {
    const {
      damageToRightFighter,
      damageToLeftFighter,
      rightHealthValue,
      leftHealthValue,
      rightHealth,
      leftHealth,
    } = this.state;
    if (PlayerOneCriticalHitCombination.includes(code)) {
      const damage = this.getCriticalHit(firstFighter);
      const healthValue = rightHealthValue - (damage / rightHealthValue) * 100;
      this.setState({ damageToRightFighter: damage, rightHealthValue: healthValue });
      this.setHealth(secondFighter, damageToRightFighter, rightHealth, rightHealthValue);
      this.animateAttack(this.leftFighter, 'animate');
    } else if (PlayerTwoCriticalHitCombination.includes(code)) {
      const damage = this.getCriticalHit(secondFighter);
      const healthValue = leftHealthValue - (damage / leftHealthValue) * 100;
      this.setState({ damageToLeftFighter: damage, leftHealthValue: healthValue });
      this.setHealth(firstFighter, damageToLeftFighter, leftHealth, leftHealthValue);
      this.animateAttack(this.rightFighter, 'animate-right');
    }
  };

  handleKeyPressEvent = (e) => {
    let { player1, player2, pressedKeys, interval } = this.state;
    if (e.type === 'keydown') {
      this.setState({ pressedKeys: pressedKeys.add(e.code) });
    }
    if (e.type === 'keyup') {
      this.hit(player1, player2);
      this.controlHealth(player1, player2);
      if (interval === 10 && pressedKeys.size === 3) {
        this.criticalHit(player1, player2, e.code);
        let timer = setInterval(() => {
          interval--;
          if (interval === 0) {
            clearInterval(timer);
            this.setState({ innterval: 10 });
          }
        }, 1000);
      }
      this.setState({ pressedKeys: pressedKeys.delete(e.code) });
    }
  };

  render() {
    // const { player1, player2 } = this.state;
    return (
      <>
        <Arena fighterOne={this.props.fighterOne} fighterTwo={this.props.fighterTwo} />;
        <input
          type="hidden"
          onKeyDown={this.handleKeyPressEvent}
          onKeyUp={this.handleKeyPressEvent}
        />
      </>
    );
  }
}

export default Hits;
