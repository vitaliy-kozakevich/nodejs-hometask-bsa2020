import React, { useState } from 'react';
import { FormControl, InputLabel, makeStyles, Select } from '@material-ui/core';
import { MenuItem } from 'material-ui';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

export default function Fights({ fightsList, onFightSelect, selectedFight }) {
  const classes = useStyles();
  const [fight, setFight] = useState();

  const handleChange = (event) => {
    debugger;
    setFight(event.target.value);
    onFightSelect(event.target.value);
  };

  return (
    <div>
      <FormControl className={classes.formControl}>
        <InputLabel id="simple-select-label">Select Fight</InputLabel>
        <Select
          labelId="simple-select-label"
          id="simple-select"
          value={fight}
          onChange={handleChange}
        >
          {fightsList.map((it, index) => {
            return (
              <MenuItem key={`${index}`} value={it}>
                {it.id}
              </MenuItem>
            );
          })}
        </Select>
        {selectedFight ? (
          <div>
            <div>Id: {selectedFight.id}</div>
            <div>Fighter1: {selectedFight.fighter1}</div>
            <div>Fighter2: {selectedFight.fighter2}</div>
          </div>
        ) : null}
      </FormControl>
    </div>
  );
}
