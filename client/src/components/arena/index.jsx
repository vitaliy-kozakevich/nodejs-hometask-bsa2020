import React from 'react';
import './arena.css';

export default function Arena({ fighterOne, fighterTwo }) {
  const { name: name1, sourse: sourse1 } = fighterOne;
  const { name: name2, sourse: sourse2 } = fighterTwo;

  return (
    <div className="arena___root">
      <div className="arena___fight-status">
        <div className="arena___fighter-indicator">
          <span className="arena___fighter-name">{name1}</span>
          <div className="arena___health-indicator">
            <div className="arena___health-bar" id="left-fighter-indicator"></div>
          </div>
        </div>
        <div className="arena___versus-sign"></div>
        <div className="arena___fighter-indicator">
          <span className="arena___fighter-name">{name2}</span>
          <div className="arena___health-indicator">
            <div className="arena___health-bar" id="right-fighter-indicator"></div>
          </div>
        </div>
      </div>
      <div className="arena___battlefield">
        <div className="arena___fighter arena___left-fighter">
          <img className="fighter-preview___img" src={sourse1} alt={name1} />
        </div>
        <div className="arena___fighter arena___right-fighter">
          <img className="fighter-preview___img" src={sourse2} alt={name2} />
        </div>
      </div>
    </div>
  );
}
