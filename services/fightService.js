const { FightRepository } = require('../repositories/fightRepository');

class FightService {
  // OPTIONAL TODO: Implement methods to work with fights
  async search(search) {
    const item = await FightRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  async getAllFights() {
    const items = await FightRepository.getAll();
    if (!items.length) {
      return Promise.reject('Fights have not been found!');
    }
    return items;
  }

  async createFight(user) {
    const item = await FightRepository.create(user);
    if (!item) {
      return Promise.reject('Fight has not been created!');
    }
    return item;
  }

  async updateFight(search, updateInfo) {
    const item = await FightRepository.update(search, updateInfo);
    if (!item) {
      const { id } = search;
      return Promise.reject(`Fight has not been found by id: ${id}!`);
    }
    return item;
  }

  async deleteFight(search) {
    const item = await FightRepository.delete(search);
    if (!item.length) {
      const { id } = search;
      return Promise.reject(`Fight has not been deleted by id: ${id}!`);
    }
    return item;
  }
}

module.exports = new FightService();
