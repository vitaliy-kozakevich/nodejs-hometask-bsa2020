const UserService = require('./userService');

class AuthService {
    async login(userData) {
        const user = await UserService.search(userData);
        if(!user) {
           return Promise.reject('User with such email is not exist');
        }
        return user;
    }
}

module.exports = new AuthService();