const { UserRepository } = require('../repositories/userRepository');

class UserService {
  // TODO: Implement methods to work with user
  async deleteUser(search) {
    const item = await UserRepository.delete(search);
    if (!item.length) {
      const { id } = search;
      return Promise.reject(`User has not been deleted by id: ${id}!`);
    }
    return item;
  }

  async updateUser(search, updateInfo) {
    const item = await UserRepository.update(search, updateInfo);
    if (!item) {
      const { id } = search;
      return Promise.reject(`User has not been found by id: ${id}!`);
    }
    return item;
  }

  async createUser(user) {
    const item = await UserRepository.create(user);
    if (!item) {
      return Promise.reject('User has not been created!');
    }
    return item;
  }

  async getAllUsers() {
    const items = await UserRepository.getAll();
    if (!items.length) {
      return Promise.reject('Users have not been found!');
    }
    return items;
  }

  async search(search) {
    const item = await UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new UserService();
