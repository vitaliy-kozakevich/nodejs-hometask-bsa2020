const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
  // TODO: Implement methods to work with fighters
  async search(search) {
    const item = await FighterRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  async getAllUsers() {
    const items = await FighterRepository.getAll();
    if (!items.length) {
      return Promise.reject('Fighters have not been found!');
    }
    return items;
  }

  async createUser(user) {
    const item = await FighterRepository.create(user);
    if (!item) {
      return Promise.reject('Fighter has not been created!');
    }
    return item;
  }

  async updateUser(search, updateInfo) {
    const item = await FighterRepository.update(search, updateInfo);
    if (!item) {
      const { id } = search;
      return Promise.reject(`Fighter has not been found by id: ${id}!`);
    }
    return item;
  }

  async deleteUser(search) {
    const item = await FighterRepository.delete(search);
    if (!item.length) {
      const { id } = search;
      return Promise.reject(`Fighter has not been deleted by id: ${id}!`);
    }
    return item;
  }
}

module.exports = new FighterService();
