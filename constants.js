const RES_CODES = {
  ok: 200,
  created: 201,
  bad_request: 400,
  not_found: 404,
};

module.exports = {
  RES_CODES,
};
