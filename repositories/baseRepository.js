const { dbAdapter } = require('../config/db');
const { v4 } = require('uuid');

class BaseRepository {
  constructor(collectionName) {
    this.dbContext = dbAdapter.get(collectionName);
    this.collectionName = collectionName;
  }

  generateId() {
    return v4();
  }

  async getAll() {
    return this.dbContext.value();
  }

  async getOne(search) {
    return this.dbContext.find(search).value();
  }

  async create(data) {
    data.id = this.generateId();
    data.createdAt = new Date();
    const list = this.dbContext.push(data).write();
    return list.find((it) => it.id === data.id);
  }

  async update(id, dataToUpdate) {
    dataToUpdate.updatedAt = new Date();
    return this.dbContext.find(id).assign(dataToUpdate).write();
  }

  async delete(id) {
    return this.dbContext.remove(id).write();
  }
}

exports.BaseRepository = BaseRepository;
