const { Router } = require('express');
const FightService = require('../services/fightService');
const { createFightValid, updateFightValid } = require('../middlewares/fight.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { RES_CODES } = require('../constants');

const router = Router();

// OPTIONAL TODO: Implement route controller for fights
router.get(
  '/',
  async (req, res, next) => {
    try {
      const fighters = await FightService.getAllFights();
      res.stat = RES_CODES.ok;
      res.data = fighters;
    } catch (error) {
      res.stat = RES_CODES.not_found;
      res.err = error;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  '/:id',
  async (req, res, next) => {
    try {
      const {
        params: { id },
      } = req;
      const fighter = await FightService.search({ id });
      if (!fighter) {
        throw Error('User has not been found!');
      }
      res.stat = RES_CODES.ok;
      res.data = fighter;
    } catch (error) {
      res.status = RES_CODES.not_found;
      res.err = error;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.post(
  '/',
  createFightValid,
  async (req, res, next) => {
    try {
      const fighter = await FightService.createFight(req.body);
      res.stat = RES_CODES.ok;
      res.data = fighter;
    } catch (error) {
      res.stat = RES_CODES.bad_request;
      res.err = error;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.put(
  '/:id',
  updateFightValid,
  async (req, res, next) => {
    try {
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        res.stat = RES_CODES.bad_request;
        res.err = errors.array()[0].msg;
        return;
      }

      const {
        params: { id },
      } = req;
      const user = await FightService.updateFight({ id }, req.body);
      res.stat = RES_CODES.ok;
      res.data = user;
    } catch (error) {
      res.stat = RES_CODES.bad_request;
      res.err = error;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.delete(
  '/:id',
  async (req, res, next) => {
    try {
      const {
        params: { id },
      } = req;
      const user = await FightService.deleteFight({ id });
      res.stat = RES_CODES.ok;
      res.data = user;
    } catch (error) {
      res.stat = RES_CODES.bad_request;
      res.err = error;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
