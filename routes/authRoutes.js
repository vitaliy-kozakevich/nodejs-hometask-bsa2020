const { Router } = require('express');
const bcrypt = require('bcryptjs');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { RES_CODES } = require('../constants');

const router = Router();

router.post(
  '/login',
  async (req, res, next) => {
    try {
      // TODO: Implement login action (get the user if it exist with entered credentials)
      const { email, password } = req.body;
      const candidate = await AuthService.login({ email });
      const areSame = await bcrypt.compare(password, candidate.password);
      if (areSame) {
        res.stat = RES_CODES.ok;
        res.data = candidate;
      } else {
        res.stat = RES_CODES.bad_request;
        res.err = 'Password is incorrect';
      }
    } catch (err) {
      res.stat = RES_CODES.bad_request;
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
