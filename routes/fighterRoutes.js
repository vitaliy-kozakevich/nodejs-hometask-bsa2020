const { Router } = require('express');
const { validationResult } = require('express-validator');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const {
  createFighterValid,
  updateFighterValid,
  isNameExistValidator,
} = require('../middlewares/fighter.validation.middleware');
const { RES_CODES } = require('../constants');

const router = Router();

// TODO: Implement route controllers for fighter
router.get(
  '/',
  async (req, res, next) => {
    try {
      const fighters = await FighterService.getAllUsers();
      res.stat = RES_CODES.ok;
      res.data = fighters;
    } catch (error) {
      res.stat = RES_CODES.not_found;
      res.err = error;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  '/:id',
  async (req, res, next) => {
    try {
      const {
        params: { id },
      } = req;
      const fighter = await FighterService.search({ id });
      if (!fighter) {
        throw Error('User has not been found!');
      }
      res.stat = RES_CODES.ok;
      res.data = fighter;
    } catch (error) {
      res.status = RES_CODES.not_found;
      res.err = error;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.post(
  '/',
  isNameExistValidator,
  createFighterValid,
  async (req, res, next) => {
    try {
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        res.stat = RES_CODES.bad_request;
        res.err = errors.array()[0].msg;
        return;
      }

      const fighter = await FighterService.createUser(req.body);
      res.stat = RES_CODES.ok;
      res.data = fighter;
    } catch (error) {
      res.stat = RES_CODES.bad_request;
      res.err = error;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.put(
  '/:id',
  isNameExistValidator,
  updateFighterValid,
  async (req, res, next) => {
    try {
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        res.stat = RES_CODES.bad_request;
        res.err = errors.array()[0].msg;
        return;
      }

      const {
        params: { id },
      } = req;
      const user = await FighterService.updateUser({ id }, req.body);
      res.stat = RES_CODES.ok;
      res.data = user;
    } catch (error) {
      res.stat = RES_CODES.bad_request;
      res.err = error;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.delete(
  '/:id',
  async (req, res, next) => {
    try {
      const {
        params: { id },
      } = req;
      const user = await FighterService.deleteUser({ id });
      res.stat = RES_CODES.ok;
      res.data = user;
    } catch (error) {
      res.stat = RES_CODES.bad_request;
      res.err = error;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
