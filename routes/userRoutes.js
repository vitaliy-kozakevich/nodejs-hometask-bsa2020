const { Router } = require('express');
const bcrypt = require('bcryptjs');
const { check, validationResult } = require('express-validator');
const UserService = require('../services/userService');
const {
  createUserValid,
  updateUserValid,
  registerValidators,
} = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { RES_CODES } = require('../constants');

const router = Router();

// TODO: Implement route controllers for user

//get all users
router.get(
  '/',
  async (req, res, next) => {
    try {
      const users = await UserService.getAllUsers();
      res.stat = RES_CODES.ok;
      res.data = users;
    } catch (error) {
      res.stat = RES_CODES.not_found;
      res.err = error;
    } finally {
      next();
    }
  },
  responseMiddleware
);

// get user by id
router.get(
  '/:id',
  async (req, res, next) => {
    try {
      const {
        params: { id },
      } = req;
      const user = await UserService.search({ id });
      if (!user) {
        throw Error('User has not been found!');
      }
      res.stat = RES_CODES.ok;
      res.data = user;
    } catch (error) {
      res.status = RES_CODES.not_found;
      res.err = error;
    } finally {
      next();
    }
  },
  responseMiddleware
);

// create user
router.post(
  '/',
  registerValidators,
  createUserValid,
  async (req, res, next) => {
    try {
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        res.stat = RES_CODES.bad_request;
        res.err = errors.array()[0].msg;
        return;
      }

      const { password } = req.body;
      const hashedPassword = await bcrypt.hash(password, 12);

      const user = await UserService.createUser({
        ...req.body,
        password: hashedPassword,
      });
      res.stat = RES_CODES.ok;
      res.data = user;
    } catch (error) {
      res.stat = RES_CODES.bad_request;
      res.err = error;
    } finally {
      next();
    }
  },
  responseMiddleware
);

// update user by id
router.put(
  '/:id',
  [
    check('password', 'Minimal password length — 6 symbols')
      .isLength({ min: 6 })
      .isAlphanumeric()
      .trim(),
    check('firstName')
      .isLength({ min: 3 })
      .withMessage('First name must be 3 symbols or more')
      .trim(),
    check('lastName')
      .isLength({ min: 3 })
      .withMessage('Last name must be 3 symbols or more')
      .trim(),
  ],
  updateUserValid,
  async (req, res, next) => {
    try {
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        res.stat = RES_CODES.bad_request;
        res.err = errors.array()[0].msg;
        return;
      }

      const {
        params: { id },
      } = req;

      const { password } = req.body;
      const hashedPassword = await bcrypt.hash(password, 12);

      const user = await UserService.updateUser({ id }, {
        ...req.body,
        password: hashedPassword,
      });
      res.stat = RES_CODES.ok;
      res.data = user;
    } catch (error) {
      res.stat = RES_CODES.bad_request;
      res.err = error;
    } finally {
      next();
    }
  },
  responseMiddleware
);

// delete user by id
router.delete(
  '/:id',
  async (req, res, next) => {
    try {
      const {
        params: { id },
      } = req;
      const user = await UserService.deleteUser({ id });
      res.stat = RES_CODES.ok;
      res.data = user;
    } catch (error) {
      res.stat = RES_CODES.bad_request;
      res.err = error;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
